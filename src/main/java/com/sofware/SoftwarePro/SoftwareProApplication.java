package com.sofware.SoftwarePro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftwareProApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftwareProApplication.class, args);
	}

}
