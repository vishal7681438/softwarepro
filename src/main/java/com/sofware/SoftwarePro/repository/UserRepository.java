package com.sofware.SoftwarePro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sofware.SoftwarePro.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>  {

}
