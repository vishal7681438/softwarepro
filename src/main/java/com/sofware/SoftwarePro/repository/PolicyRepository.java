package com.sofware.SoftwarePro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sofware.SoftwarePro.entity.Policy;

public interface PolicyRepository extends JpaRepository<Policy,Integer>{

}
