package com.sofware.SoftwarePro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sofware.SoftwarePro.entity.Claim;

public interface ClaimRepository extends JpaRepository<Claim,Integer> {

}
