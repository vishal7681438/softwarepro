package com.sofware.SoftwarePro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sofware.SoftwarePro.entity.Claim;
import com.sofware.SoftwarePro.service.ClaimService;

@RestController
@RequestMapping("/claim")
public class ClaimRestController {
	
	@Autowired
	private ClaimService claimService;
	
	@PostMapping("/claims")
	public ResponseEntity<Claim> claims(Claim claim) {
		
		return null;
	}
	
	@GetMapping("/claims/{claimId}")
	public ResponseEntity<Claim> getClaimDetailsById(Integer claimId) {
		
		return null;
	}
	

}
