package com.sofware.SoftwarePro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sofware.SoftwarePro.entity.Policy;
import com.sofware.SoftwarePro.service.PolicyService;

@Controller
@RequestMapping("policy")
public class InsuranceController {
	
	@Autowired
	private PolicyService policyService;
	
	@GetMapping("/policies")
	public ResponseEntity<List<Policy>> getAllPolicies() {
		
		return null;
	}
	
	@PostMapping("/buy-policy/{policyId}")
	public ResponseEntity<Policy> buyPolicy(Integer policyId) {
		
		return null;
	}
	
	@GetMapping("/my-policies")
	public ResponseEntity<Policy> getPolicy() {
		
		return null;
		
	}

}
