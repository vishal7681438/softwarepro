package com.sofware.SoftwarePro.service;

import java.util.List;

import com.sofware.SoftwarePro.entity.User;

public interface UserService {
	
//	Register a New User
	User registerUser(User user);
	
//	Get User By Name
	User getUserByName(String userName);
	
//	Get User By Id
	User getUserById(Integer userId);
	
//	update User Details
	User updateUser(User user);
	
//	get All User Detail
	List<User> getAllUsers();
	
//	delete User
	void deleteUser(Integer userId);
	
}
