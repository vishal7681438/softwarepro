package com.sofware.SoftwarePro.service;

import java.util.List;

import com.sofware.SoftwarePro.entity.Policy;
import com.sofware.SoftwarePro.entity.User;

public interface PolicyService {
	
//	Create a New Policy
	Policy createPolicy(Policy policy);
	
//	retrieve policy by Id
	Policy getPolicyById(Integer policyId);
	
//	retrieve All Policy
	List<Policy> getAllPolicies();
	
//	Buy Policy
	Policy buyPolicy(User user, Policy policy);
	
//	retrieve policy by user
	Policy getUserPolicy(User user);

}
