package com.sofware.SoftwarePro.service;

import java.util.List;
import com.sofware.SoftwarePro.entity.Claim;
import com.sofware.SoftwarePro.entity.ClaimStatus;
import com.sofware.SoftwarePro.entity.User;

public interface ClaimService {
	
	Claim createClaim(Claim claim);
	
	Claim getClaimById(Integer claimId);
	
	Claim updateClaim(Claim claim);
	
	List<Claim> getAllClaim();
	
	Claim getClaimByUser(User user);
	
	ClaimStatus approveClaim(Integer claimId);
	

}
