package com.sofware.SoftwarePro.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; 
	@NotNull(message = "Field must be Required")
	private String userName;
	@Email(regexp = ".+@gmail\\.com" , message = "Please Provide Valid Email")
	private String email;
	@NotBlank(message = "not blank")
	private String password;
	@OneToMany(mappedBy = "policyHolder",cascade = CascadeType.ALL)
	private List<Policy> policies;
	@OneToMany(mappedBy = "user",cascade = CascadeType.ALL)
	private List<Claim> claims;
	
}
